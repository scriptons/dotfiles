# Usage

## nvim-treesitter

Install typescript-langage-sever :

npm install -g typescript-language-server typescript

## fonts

Extract fonts to ~/.fonts

Update with : fc-cache -f -v

## Configure terminal

Choose wich font to use and set terminal for use it

## Luasnip

If jsregexp compile doesn't work when installing, go to the luasnip folder and do make by hand.

May need luarocks


