" Allow us to use Ctrl-s and Ctrl-q as keybinds
silent !stty -ixon

" Restore default behaviour when leaving Vim.
 autocmd VimLeave * silent !stty ixon

" Automatic vim-plug install
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs
  \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Manage plugins with vim-plug.
call plug#begin()

  " Status line
  Plug 'nvim-lualine/lualine.nvim'      

  " Icons
  Plug 'nvim-tree/nvim-web-devicons'    

  " dashboard
  Plug 'glepnir/dashboard-nvim'         

  " Terminal
  Plug 'akinsho/toggleterm.nvim', {'tag' : '2.3.0'}            

  " Registers
  Plug 'junegunn/vim-peekaboo'          

  " Easymotion replacement
  Plug 'phaazon/hop.nvim'               

  " Theme
  Plug 'sainnhe/sonokai'                

  " () {} etc.
  Plug 'windwp/nvim-autopairs'          

  " Git
  Plug 'tpope/vim-fugitive'             

  " Highlighting, Folding, etc.
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}   

  " file viewer
  Plug 'nvim-tree/nvim-tree.lua'        

  " LSP, Lint, Format, DAP
  Plug 'neovim/nvim-lspconfig'          " LSP
  Plug 'mfussenegger/nvim-dap'          " DAP
  Plug 'mfussenegger/nvim-lint'         " Lint
  Plug 'mhartington/formatter.nvim'     " Formatter
  Plug 'williamboman/mason.nvim'        " Manage LSP, DAP, Lint, Formatter
  " closes some gaps that exist between mason.nvim and lspconfig
  Plug 'williamboman/mason-lspconfig.nvim'  

  " Telescope
  Plug 'nvim-lua/plenary.nvim'          " Useful lua functions
  Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.x' }
  Plug 'natecraddock/telescope-zf-native.nvim'
  Plug 'nvim-telescope/telescope-file-browser.nvim'
  Plug 'nvim-telescope/telescope-project.nvim'

  " Luasnip and VSCode like snippets
  " follow latest release and install jsregexp.
  Plug 'L3MON4D3/LuaSnip', {'tag': 'v1.2.*', 'do': 'make install_jsregexp'}
  Plug 'rafamadriz/friendly-snippets'

call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins setup and configuration

" Plug 'sainnhe/sonokai'
let g:sonokai_style = 'andromeda'
colorscheme sonokai

" Lua plugins

lua << EOF
-- Plug 'nvim-lualine/lualine.nvim'
  require('lualine').setup{
    options = {
      theme = 'powerline_dark',
    },
  }

-- Plug 'glepnir/dashboard-nvim'
  require('dashboard').setup{}

-- Plug 'phaazon/hop.nvim'
  require('hop').setup{}

-- Plug 'akinsho/toggleterm.nvim'
  -- Lazygit custom layout
  local Terminal  = require('toggleterm.terminal').Terminal
  local lazygit = Terminal:new({
    cmd = "lazygit",
    dir = "git_dir",
    direction = "float",
    float_opts = {
      border = "double",
    },
    -- function to run on opening the terminal
    on_open = function(term)
      vim.cmd("startinsert!")
      vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", {noremap = true, silent = true})
    end,
    -- function to run on closing the terminal
    on_close = function(term)
      vim.cmd("startinsert!")
    end,
  })

  function _lazygit_toggle()
    lazygit:toggle()
  end

  vim.api.nvim_set_keymap("n", "<leader>g", "<cmd>lua _lazygit_toggle()<CR>", {noremap = true, silent = true})
 
  require("toggleterm").setup{
    open_mapping = [[<A-n>]],
    insert_mapping = true,
    direction = 'horizontal',
  }

-- Plug 'windwp/nvim-autopairs'
  require("nvim-autopairs").setup {}

-- Plug 'nvim-treesitter/nvim-treesitter'
  require('nvim-treesitter.configs').setup {
    parser_install_dir = "$HOME/.local/share/nvim/parsers",
    ensure_installed = { "javascript", "css", "scss", "html", "vim" },
    sync_install = false,
    vim.opt.runtimepath:append("$HOME/.local/share/nvim/parsers"),
    highlight = {
      enable = true,
      },
  }

-- Plug 'nvim-tree/nvim-tree.lua'
  require("nvim-tree").setup {
    view = {
      width = {
        max = 30
        },
      },

    tab = {
      sync = {
        open = true
        },
      },
    }

-- Plug 'williamboman/mason.nvim'
  require("mason").setup{}

-- Plug 'neovim/nvim-lspconfig'
  local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
  end
  require('lspconfig').tsserver.setup{ on_attach = on_attach }
  
-- Plug 'nvim-telescope/telescope.nvim'
-- Mappings: Find files using Telescope command-line sugar.
  local builtin = require('telescope.builtin')
  vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
  vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
  vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
  vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
-- File_browser mapping
  vim.api.nvim_set_keymap(
    'n',
    '<space>fb',
    ':Telescope file_browser<cr>',
    { noremap = true }
  )

-- Project browser
  vim.api.nvim_set_keymap(
          'n',
          '<space>fw',
          ":lua require'telescope'.extensions.project.project{}<CR>",
          {noremap = true, silent = true}
  )

  require('telescope').setup {}
  require('telescope').load_extension('zf-native')
  require('telescope').load_extension('file_browser')
  require('telescope').load_extension('project')
  --require('telescope').extensions.project.project{}

  -- Luasnip
  require("luasnip.loaders.from_vscode").lazy_load()

EOF


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has('termguicolors')
  set termguicolors
endif

" Line number
set number
" Hide buffers instead of close them
set hidden

" Indentation, tabs and spaces
set autoindent " Respect indentation when starting a new line.
set expandtab  " Expand tabs to spaces. Essential in Python.
set tabstop=2  " Number of spaces tab is counted for.
set shiftwidth=2 " Number of spaces to use for autoindent.

" Folding
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set nofoldenable    " Disable folding at startup
set foldcolumn=4    " Folding marks"

" Highlighing yank
au TextYankPost * silent! lua vim.highlight.on_yank{timeout=500}

" Set up swap files outside of current directory
let mySwapDir = expand($HOME . "/.config/nvim/.swp//")
if !isdirectory($mySwapDir)
    call mkdir($mySwapDir, "p")
endif
let &directory = mySwapDir
silent! set directory

" Set up persistent undo across all files.
set undofile
let myUndoDir = expand($HOME . "/.config/nvim/.undo//")
if !isdirectory($myUndoDir)
	call mkdir($myUndoDir, "p", "700")
endif
let &undodir = myUndoDir
silent! set undodir

" Mapping
let mapleader = " "

" Edit and source init.vim
nnoremap <leader>ev :tabnew $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" Quit without saving
inoremap <C-q> <esc>:q!<cr>
nnoremap <C-q> :q!<cr>

" New tab
nnoremap <C-t> :tabnew<cr>

" Motion for words and characters
nnoremap <A-w> :HopWord<cr>
nnoremap <C-c> :HopChar1<cr>

" Open nvim-tree
noremap <A-t> :NvimTreeOpen<cr>

" Dashboard
nnoremap <A-d> :Dashboard<cr>

" Toggel mason: Install LSP, Linter, DAB and Formatter
nnoremap <A-m> :Mason<cr>

" Tabs navigation
nnoremap <C-Right> :tabnext<cr>
nnoremap <C-Left> :tabprevious<cr>

" Windows navigation
nnoremap <C-l> :winc l<cr>
nnoremap <C-h> :winc h<cr>

" Escape from terminal mode
tnoremap <ESC> <C-\><C-N>
" Launch a tab terminal
nnoremap <leader>t :ToggleTerm dir=. direction=tab<cr>

" nvim-lspconfig
inoremap <C-e> <C-x><C-o>

" Luasnip keymaps
" press <Tab> to expand or jump in a snippet. These can also be mapped separately
" via <Plug>luasnip-expand-snippet and <Plug>luasnip-jump-next.
imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>'
